<?php session_start(); ?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Page_ConsultationCommandes.php</title>
  </head>
  <body>
    <div class="jumbotron jumbotron-fluid" style="background-color:darkgrey;text-align:center;margin-bottom:0;padding-top:20px;padding-bottom:25px;color:black;border:2px solid black;">
	  <img src="images/epicerie.jpg" class="" width="120" height="120" style="position:absolute;right:30px;top:10px;border:2px solid black;">
	  <img src="images/epicerie.jpg" class="" width="120" height="120" style="position:absolute;left:30px;top:10px;border:2px solid black;">
		<h1>Epicerie Test SA</h1>
		<h4>Bienvenue / Welcome / Willkommen</h4>
	</div>
	
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top" style="margin:0;padding:0;margin-top:0;">
			
			<button class="navbar-toggler" data-toggle="collapse" data-target="#navbarSupportedContent" style="margin:0;">
				<span class="navbar-toggler-icon"></span>
			</button>
		
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav">
					<li class="nav-item">
						<a class="nav-link" href="Page_Accueil.html"><i class="fas fa-home"></i> Home</a>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" data-toggle="dropdown" data-target="dropdown_target" href="#">Produits</a>
						<div class="dropdown-menu" aria-labelledby="dropdown_target">
							<a class="dropdown-item" href="Boissons.php">Boissons</a>
							<a class="dropdown-item" href="Bonbons.php">Bonbons</a>
							<a class="dropdown-item" href="Tabacs.php">Tabacs</a>
							<a class="dropdown-item" href="Sucres.php">Sucrés</a>
							<a class="dropdown-item" href="Sales.php">Salés</a>
						</div>
					</li>
				  <li class="nav-item">
					<a class="nav-link" href="Page_EffectuerCommande.php">Passer une commande</a>
				  </li>
					<li class="nav-item">
					  <a class="nav-link" href="Page_Contact.html">Contact</a>
				  	</li>
				</ul>	
			</div>
		</nav>
	
	<?php
		try{
			
			echo "<div class='container-fluid pt-3'>";
			
			$bdd = new PDO("mysql:host=hhva.myd.infomaniak.com;dbname=hhva_marcbrnt", "hhva_marcbrnt", "nFw9BHduqf");
				
			$bdd->query("SET NAMES 'utf8'");
			
			$idClient = $_SESSION['idClient'];
			
			$reponseDeClients = $bdd->query("SELECT * FROM client WHERE CLI_ID = '$idClient'");
			$donneesDeClients = $reponseDeClients -> fetch();
			
			echo $donneesDeClients['CLI_NOM'] . " " . $donneesDeClients['CLI_PRENOM'] . "<br />";
			echo $donneesDeClients['CLI_ADRESSE'] . "<br />";
			echo $donneesDeClients['CLI_NPA'] . " " . $donneesDeClients['CLI_LOCALITE'] . "<br />";
			echo "<hr />";
			
			$reponseDeCommandes = $bdd->query("SELECT * FROM commande WHERE COM_CLI_ID = '$idClient'");
			$n = $reponseDeCommandes->rowCount();
			
			if ($n == 0) {
				echo "<h4>Ce client n'a passé aucune commande.</h4>";
			}
			else {
				echo "<h4>Récapitulatif de vos commandes:</h4>";
				
				echo "<hr />";
				
				$numeroDeCommande = 1;
				
				while ($donneesDeCommandes = $reponseDeCommandes->fetch()) {
					echo "<h4>Commande numéro " . $numeroDeCommande . "</h4>";
					
					$total = 0;
					
					$idCommande = $donneesDeCommandes['COM_ID'];
					
					$reponseDecommandes_has_articles = $bdd->query("SELECT * FROM compro WHERE COM_ID = '$idCommande'");
					
					while ($donneesDecommandes_has_articles = $reponseDecommandes_has_articles->fetch()) {
						$idProduit = $donneesDecommandes_has_articles['PRO_ID'];
						
						$reponseDeArticles = $bdd->query("SELECT * FROM produit WHERE PRO_ID = '$idProduit'");
						$donneesDeArticles = $reponseDeArticles->fetch();
						
						echo $donneesDeArticles['PRO_GRO_ID'] . ", " . $donneesDeArticles['PRO_DESCRIPTION'] . ", " . $donneesDeArticles['PRO_PRIX'] . " CHF l'unité, " . $donneesDecommandes_has_articles['COMPRO_QUANTITE'] . " unité-s<br/>";

						$total = $total + $donneesDeArticles['PRO_PRIX'] * $donneesDecommandes_has_articles['COMPRO_QUANTITE'];	
					}
					
					echo "<br />";
					
					echo "Pour un montant total de: " . number_format($total, 2) . " CHF.";
					
					echo "<hr />";
					
					$numeroDeCommande = $numeroDeCommande + 1;
				}
			}	
				
			$bdd = null;
			
			echo "<br />";	
			echo "</div>";
		
			
		}catch(PDOException $e){
			echo "Erreur !: " . $e->getMessage() . "<br />";
			die();
		}
	?>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>