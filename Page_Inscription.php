<!DOCTYPE html><!-- Spécifie un document HTML 5 -->
<html>
	<head><!-- En-tête de la page -->
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
		<!-- Lien pour le téléchargement (CDN) de code jQuery -->
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<!-- Lien pour le téléchargement (CDN) de la feuille de style de Bootstrap !! -->		
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<!-- Lien pour le téléchargement (CDN) du code JavaScript -->
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
		<title>Page_Inscription.php</title>
	</head>	
	<body>
		<?php
			if ($_POST['Motdepasse'] == $_POST['ConfirmationMotdepasse']) {
				$Nom = strtoupper($_POST['Nom']);
				$Prenom = ucwords($_POST['Prenom']);
				$Adresse = $_POST['Adresse'];
				$Codepostal = $_POST['Codepostal'];
				$Localite = $_POST['Localite'];
				$DatedenaissanceClient = $_POST['Datedenaissance'];
				
				$dateDeNaissance = explode("/", $DatedenaissanceClient);
				
				$dateDeNaissancePourSQL = $dateDeNaissance[2] . "-" . $dateDeNaissance[1] . "-" . $dateDeNaissance[0];
				
				$Courriel = $_POST['Courriel'];
				$Motdepasse = $_POST['Motdepasse'];
				$Portable = $_POST['Portable'];
				
				try {
					$bdd = new PDO("mysql:host=hhva.myd.infomaniak.com;dbname=hhva_marcbrnt", "hhva_marcbrnt", "nFw9BHduqf");
					
					$bdd->query("SET NAMES 'utf8'");
					
					$reponseDeClients = $bdd->query("SELECT * FROM client WHERE CLI_EMAIL = '$Courriel'");
					$n = $reponseDeClients->rowCount();
					
					if ($n == 0) {
						$bdd->query("INSERT INTO client(cli_nom, cli_prenom, cli_adresse, cli_npa, cli_localite, cli_dob, cli_email, cli_mdp, cli_numero_telephone) VALUES('$Nom', '$Prenom', '$Adresse', '$Codepostal', '$Localite', '$dateDeNaissancePourSQL', '$Courriel', '$Motdepasse', '$Portable')");
						
						?>
							<script type="text/javascript">
								alert("Vos données ont été enregistrées. Vous pouvez maintenant vous connecter.");
								document.location.href = "Page_Connexion.html";	
							</script>
						<?php
						
					}
					else{
						?>
							<script type="text/javascript">
								alert("Un client avec la même adresse courriel que la vôtre est déjà enregistré. Enregistrement impossible !");
								document.location.href = "Page_Connexion.html";	
							</script>
						<?php
					}
					
					$bdd = null;
				}
				catch (PDOException $e) {
					echo "Erreur !: " . $e->getMessage() . "<br />";
					die();
				}		
			}
			else {
				?>
					<script type="text/javascript">
						alert("Les mots de passe que vous avez tapés ne sont pas identiques. Recommencez.");
						document.location.href = "Page_Inscription.html";	
					</script>
				<?php
			}
			?>
	</body>
</html>