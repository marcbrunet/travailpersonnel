<?php session_start(); ?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Enregistrer la commande</title>
  </head>
  <body>
	<div class="jumbotron jumbotron-fluid" style="background-color:darkgrey;text-align:center;margin-bottom:0;padding-top:20px;padding-bottom:25px;color:black;border:2px solid black;">
	  <img src="images/epicerie.jpg" class="" width="120" height="120" style="position:absolute;right:30px;top:10px;border:2px solid black;">
	  <img src="images/epicerie.jpg" class="" width="120" height="120" style="position:absolute;left:30px;top:10px;border:2px solid black;">
	  <h1>Epicerie Test SA</h1>
	  <h4>Bienvenue / Welcome / Willkommen</h4>
	</div>
	
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top" style="margin:0;padding:0;margin-top:0;">
			
			<button class="navbar-toggler" data-toggle="collapse" data-target="#navbarSupportedContent" style="margin:0;">
				<span class="navbar-toggler-icon"></span>
			</button>
		
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav">
					<li class="nav-item">
						<a class="nav-link" href="Page_Accueil.html"><i class="fas fa-home"></i> Home</a>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" data-toggle="dropdown" data-target="dropdown_target" href="#">Produits</a>
						<div class="dropdown-menu" aria-labelledby="dropdown_target">
							<a class="dropdown-item" href="Boissons.php">Boissons</a>
							<a class="dropdown-item" href="Bonbons.php">Bonbons</a>
							<a class="dropdown-item" href="Tabacs.php">Tabacs</a>
							<a class="dropdown-item" href="Sucres.php">Sucrés</a>
							<a class="dropdown-item" href="Sales.php">Salés</a>
						</div>
					</li>
				  <li class="nav-item">
					  <a class="nav-link" href="Page_Contact.html">Contact</a>
				  	</li>
				</ul>
				<ul class="navbar-nav ml-auto">
					<li class="nav-item">
						<a class="btn btn-success" href="Page_Inscription.html" type="button">S'inscrire</a>
					</li>
					<li class="nav-item">
						<a class="btn btn-primary" href="Page_Connexion.html" type="button">Se connecter</a>
					</li>
				</ul>	
			</div>
		</nav>
    <?php
			if (isset($_SESSION['Panier'])) {
				try {
					$bdd = new PDO("mysql:host=hhva.myd.infomaniak.com;dbname=hhva_marcbrnt", "hhva_marcbrnt", "nFw9BHduqf");
					
					$bdd->query("SET NAMES 'utf8'");
				}
				catch (PDOException $e) {
					echo "Erreur !: " . $e->getMessage() . "<br />";
					die();
				}	
			
				$idClient = $_SESSION['idClient'];
				
				$bdd->query("INSERT INTO commande(COM_CLI_ID) VALUES('$idClient')");
				
				$idCommande = $bdd->lastInsertId();
								
				for ($i = 0; $i < $_POST['nombreDeProduitsCommandes']; $i++) {
					$idProduit = $_SESSION['Panier']['idProduit'][$i];
					$qProduit = $_SESSION['Panier']['qProduit'][$i];
					
					$bdd->query("INSERT INTO compro (COM_ID,PRO_ID,COMPRO_QUANTITE) VALUES('$idCommande', '$idProduit', '$qProduit')");
				}
				
				$bdd = null;
				
				?>
					<script type="text/javascript">
						alert("Nous vous remercions pour votre commande, elle a été enregistrée.");
						document.location.href = 'Page_Accueil.php';
					</script>
				<?php
			}
		?>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>