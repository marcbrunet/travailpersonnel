<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<style>body{}</style>
    <title>Effectuer une commande</title>
  </head>
  <body>
    <div class="jumbotron jumbotron-fluid" style="background-color:darkgrey;text-align:center;margin-bottom:0;padding-top:20px;padding-bottom:25px;color:black;border:2px solid black;">
      <img src="images/epicerie.jpg" class="" width="120" height="120" style="position:absolute;right:30px;top:10px;border:2px solid black;">
      <img src="images/epicerie.jpg" class="" width="120" height="120" style="position:absolute;left:30px;top:10px;border:2px solid black;">
      <h1>Epicerie Test SA</h1>
      <h4>Bienvenue / Welcome / Willkommen</h4>
    </div>
	
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top" style="margin:0;padding:0;margin-top:0;">
			
			<button class="navbar-toggler" data-toggle="collapse" data-target="#navbarSupportedContent" style="margin:0;">
				<span class="navbar-toggler-icon"></span>
			</button>
		
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav">
					<li class="nav-item">
						<a class="nav-link" href="Page_Accueil.html"><i class="fas fa-home"></i> Home</a>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" data-toggle="dropdown" data-target="dropdown_target" href="#">Produits</a>
						<div class="dropdown-menu" aria-labelledby="dropdown_target">
							<a class="dropdown-item" href="Boissons.php">Boissons</a>
							<a class="dropdown-item" href="Bonbons.php">Bonbons</a>
							<a class="dropdown-item" href="Tabacs.php">Tabacs</a>
							<a class="dropdown-item" href="Sucres.php">Sucrés</a>
							<a class="dropdown-item" href="Sales.php">Salés</a>
						</div>
					</li>
				  <li class="nav-item">
					<a class="nav-link" href="Page_ConsultationCommandes.php">Consulter ses commandes</a>
				  </li>
					<li class="nav-item">
					  <a class="nav-link" href="Page_Contact.html">Contact</a>
				  	</li>
				</ul>
				<ul class="navbar-nav ml-auto">
					<li class="nav-item">
						<a class="btn btn-success" href="Page_Inscription.html" type="button">S'inscrire</a>
					</li>
					<li class="nav-item">
						<a class="btn btn-primary" href="Page_Connexion.html" type="button">Se connecter</a>
					</li>
				</ul>	
			</div>
		</nav>
	

	  <div class="container-fluid">
	  <h1 class="text-center">Effectuer une commande</h1>
	  <p class="text-center text-danger">Pour valider la quantité d'un article souhaité, il vous faut absolument cocher la case sur la gauche de la description, si vous insérez une quantité et que vous ne cochez pas la case qui se trouve tout à gauche de la ligne, le produit ne sera pas comptabilisé !!</p>
	  </div>
	<?php
      try {
        $bdd = new PDO("mysql:host=hhva.myd.infomaniak.com;dbname=hhva_marcbrnt", "hhva_marcbrnt", "nFw9BHduqf");
        
        $bdd->query("SET NAMES 'utf8'");
        
        $reponseDeProduits = $bdd->query("SELECT * FROM produit");
        
        echo "<form action='Page_ConsultationCommandeEnCours.php' method='post'>";
        
		echo "<table style='margin:auto;'>";
        
        while ($donneesDeProduits = $reponseDeProduits->fetch()) {
          echo "<tr>";
          
          $IdProduit = $donneesDeProduits['PRO_ID'];
          
          $GroupeProduit = $donneesDeProduits['PRO_GRO_ID'];
          $DescriptionProduit = $donneesDeProduits['PRO_DESCRIPTION'];
          $PrixProduit = $donneesDeProduits['PRO_PRIX'];
          $QuantiteStockProduit = $donneesDeProduits['PRO_QUANTITE_STOCK'];
          
          echo "<td><input type='checkbox' name='ProNum" . $IdProduit . "' value='" . $IdProduit . "' /></td>";
          echo "<td>" . $DescriptionProduit . "</td>";
          echo "<td class='aligneADroite'>" . $PrixProduit . " CHF l'unité</td>";
          echo "<td><input type='number' name='qProNum" . $IdProduit . "' value='1' min='1' max='10' /> exemplaire-s</td>";
          
          echo "</tr>";
        }
        
        echo "</table>";
        
        echo "<br />";
        
        echo "<center><input type='reset' value='Recommencer'>";
		echo "<input type='submit' value='Commander'></center>";
        
        echo "</form>";
        
        $bdd = null;
		echo "<br><br>";
      }
      catch (PDOException $e) {
        echo "Erreur !: " . $e->getMessage() . "<br />";
        die();
      }
    ?>
	
	  <footer class="page-footer font-small bg-dark text-white mt-2 fixed-bottom">
	  <div class="footer-copyright text-center py-1">
		© 2018 Copyright
	  </div>
	  </footer>
	  
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	</body>
</html>