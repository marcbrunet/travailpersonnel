<?php session_start(); ?>
<!DOCTYPE html><!-- Spécifie un document HTML 5 -->
<html>
	<head><!-- En-tête de la page -->
		<meta charset="utf-8">		
		<title>Page_Connexion.php</title>
	</head>	
	<body>
		<?php
			try {
				$bdd = new PDO("mysql:host=hhva.myd.infomaniak.com;dbname=hhva_marcbrnt", "hhva_marcbrnt", "nFw9BHduqf");
						
				$bdd->query("SET NAMES 'utf8'");
						
				$email = $_POST['email'];
				$mdp = $_POST['mdp'];
						
				$reponseDeClients = $bdd->query("SELECT * FROM client WHERE CLI_EMAIL = '$email'");
				$nbEnregistrements = $reponseDeClients->rowCount();
						
				if ($nbEnregistrements == 0) {
					?>
						<script type="text/javascript">
							alert("Aucun utilisateur portant ce login n'est référencé. Veuillez vous enregistrer s'il vous plaît.");
							document.location.href = "Page_Inscription.html";	
						</script>
					<?php
				}
				else {
					$donneesDeClients = $reponseDeClients->fetch();
						
					if ($donneesDeClients['CLI_MDP'] == $mdp) {
						$_SESSION['idClient'] = $donneesDeClients['CLI_ID'];
						$_SESSION['nomClient'] = $donneesDeClients['CLI_NOM'];
						$_SESSION['prenomClient'] = $donneesDeClients['CLI_PRENOM'];
								
						header("Location:Page_Accueil.php");
					}
					else {
						?>
							<script type="text/javascript">
								alert("Votre mot de passe est erroné.");
								document.location.href = "Page_Connexion.html";	
							</script>
						<?php
					}
				}
						
				$bdd = null;
			}
			catch (PDOException $e) {
				echo "Erreur !: " . $e->getMessage() . "<br />";
				die();
			}	
		?>
	</body>
</html>