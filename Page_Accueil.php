<?php session_start(); ?>

<!DOCTYPE html><!-- Spécifie un document HTML 5 -->
<html>
	<head><!-- En-tête de la page -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1 shrink-to-fit=no"/>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
		<!-- Lien pour le téléchargement (CDN) de code jQuery -->
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<!-- Lien pour le téléchargement (CDN) de la feuille de style de Bootstrap !! -->		
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<!-- Lien pour le téléchargement (CDN) du code JavaScript -->
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
		
		<title>Epicerie en ligne</title>
		<style>
			html{width:100%;height:100%;margin:0;padding:0;}
			body{width:100%;height:100%;margin:0;padding:0;}			
		</style>
	</head>
	<body><!-- Corps de la page -->
	
	<div class="jumbotron jumbotron-fluid" style="background-color:darkgrey;text-align:center;margin-bottom:0;padding-top:20px;padding-bottom:25px;color:black;border:2px solid black;">
		<img src="images/epicerie.jpg" class="" width="120" height="120" style="position:absolute;right:30px;top:10px;border:2px solid black;">
		<img src="images/epicerie.jpg" class="" width="120" height="120" style="position:absolute;left:30px;top:10px;border:2px solid black;">
		<h1>Epicerie Test SA</h1>
		<h4>Bienvenue / Welcome / Willkommen</h4>
	</div>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top" style="margin:0;padding:0;margin-top:0;">
			
			<button class="navbar-toggler" data-toggle="collapse" data-target="#navbarSupportedContent" style="margin:0;">
				<span class="navbar-toggler-icon"></span>
			</button>
		
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav">
					<li class="nav-item">
						<a class="nav-link" href="Page_Accueil.html"><i class="fas fa-home"></i> Home</a>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" data-toggle="dropdown" data-target="dropdown_target" href="#">Produits</a>
						<div class="dropdown-menu" aria-labelledby="dropdown_target">
							<a class="dropdown-item" href="Boissons.php">Boissons</a>
							<a class="dropdown-item" href="Bonbons.php">Bonbons</a>
							<a class="dropdown-item" href="Tabacs.php">Tabacs</a>
							<a class="dropdown-item" href="Sucres.php">Sucrés</a>
							<a class="dropdown-item" href="Sales.php">Salés</a>
						</div>
					</li>
				  <li class="nav-item">
					<a class="nav-link" href="Page_EffectuerCommande.php">Passer une commande</a>
				  </li>
				  <li class="nav-item">
					<a class="nav-link" href="Page_ConsultationCommandes.php">Consulter ses commandes</a>
				  </li>
					<li class="nav-item">
					  <a class="nav-link" href="Page_Contact.html">Contact</a>
				  	</li>
				</ul>	
			</div>
		</nav>
	<?php
			try {
				$bdd = new PDO("mysql:host=hhva.myd.infomaniak.com;dbname=hhva_marcbrnt", "hhva_marcbrnt", "nFw9BHduqf");
						
				$bdd->query("SET NAMES 'utf8'");
				
				$idClient = $_SESSION['idClient'];
				
				$reponseDeClients = $bdd->query("SELECT * FROM client WHERE cli_id = '$idClient'");
				$donneesDeClients = $reponseDeClients->fetch();
				
			  echo "<div class='container-fluid text-black' style='margin-bottom:0;margin-top:10px;'>";
				echo "Bonjour " . $donneesDeClients['CLI_PRENOM'] . " " . strtoupper($donneesDeClients['CLI_NOM']) . "!" . "<br /><br />";
					
				echo "Vous avez la possibilité...<br />";
					
				echo "<ul style='padding:0;list-style-type:disc;list-style-position:inside;margin-bottom:0;'>";
			  	echo "<li><a href='Page_DonneesPersonnelles.php' style='color:black;'>...de consulter vos données personnelles et de les modifier si nécessaire.</a></li>";
				echo "<li><a href='Page_ConsultationCommandes.php' style='color:black;'>...de consulter vos commandes.</a></li>";
				echo "<li><a href='Page_EffectuerCommande.php' style='color:black;'>...de passer une nouvelle commande.</a></li>";
				echo "<li><a href='Page_Deconnexion.php' style='color:black;'>...de vous déconnecter.</a></li>";
				echo "</ul><br>";
				echo "</div>";
				
				$bdd = null;
			}
			catch (PDOException $e) {
				echo "Erreur !: " . $e->getMessage() . "<br />";
				die();
			}
	?>
		<footer class="page-footer font-small bg-dark text-white mt-2 fixed-bottom">
		<div class="footer-copyright text-center py-1">
		© 2018 Copyright
		</div>
		</footer>
	
	
	</body>
</html>



