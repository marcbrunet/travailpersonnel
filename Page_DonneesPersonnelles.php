<?php session_start(); ?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="calendrier.css">
	<script type="text/javascript" src="calendrier.js"></script>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Page_DonneesPersonnelles.php</title>
  </head>
  <body>
    <div class="jumbotron jumbotron-fluid" style="background-color:darkgrey;text-align:center;margin-bottom:0;padding-top:20px;padding-bottom:25px;color:black;border:2px solid black;">
		<img src="epicerie.jpg" class="" width="120" height="120" style="position:absolute;right:30px;top:10px;border:2px solid black;">
		<img src="epicerie.jpg" class="" width="120" height="120" style="position:absolute;left:30px;top:10px;border:2px solid black;">
		<h1>Epicerie Test SA</h1>
		<h4>Bienvenue / Welcome / Willkommen</h4>
	</div>
	
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top" style="margin:0;padding:0;margin-top:0;">
			
			<button class="navbar-toggler" data-toggle="collapse" data-target="#navbarSupportedContent" style="margin:0;">
				<span class="navbar-toggler-icon"></span>
			</button>
		
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav">
					<li class="nav-item">
						<a class="nav-link" href="Page_Accueil.html"><i class="fas fa-home"></i> Home</a>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" data-toggle="dropdown" data-target="dropdown_target" href="#">Produits</a>
						<div class="dropdown-menu" aria-labelledby="dropdown_target">
							<a class="dropdown-item" href="Boissons.php">Boissons</a>
							<a class="dropdown-item" href="Bonbons.php">Bonbons</a>
							<a class="dropdown-item" href="Tabacs.php">Tabacs</a>
							<a class="dropdown-item" href="Sucres.php">Sucrés</a>
							<a class="dropdown-item" href="Sales.php">Salés</a>
						</div>
					</li>
					<li class="nav-item">
					  <a class="nav-link" href="Page_Contact.html">Contact</a>
				  	</li>
				</ul>	
			</div>
		</nav>
	
	<?php 
		echo "<div class='container-fluid mt-2'>";
		echo "<h2>Voici vos données personnelles :</h2>"; 
		echo "<p class='text-danger'>Vous avez la possibilité de modifier vos données.</font></p>";
		
		try{
			$bdd = new PDO("mysql:host=hhva.myd.infomaniak.com;dbname=hhva_marcbrnt","hhva_marcbrnt","nFw9BHduqf");
			
			$bdd->query("SET NAMES 'UTF-8'");
			
			$idClient = $_SESSION['idClient'];
			
			$reponse = $bdd->query("SELECT * FROM client WHERE CLI_ID = '$idClient'");
			$donnees = $reponse->fetch();
			
		  echo "<form action='Page_DonneesPersonnellesTraitement.php' method='POST'>";
		  echo "<table style='border:2px solid black;'>";
			echo "<colgroup span=2 width='50%'";
			
			echo "<tr>";
			echo "<td><label for='Nom'>Votre nom</label></td>";
			echo "<td><input type='text' name='nom' id='Nom' alt='Saisie de votre nom' size='25' maxlength='25' value='".$donnees['CLI_NOM']."' required /></td>";
			echo "</tr>";
			
			echo "<tr>";
			echo "<td><label for='Prenom'>Votre prénom</label></td>";
			echo "<td><input type='text' name='prenom' id='Prenom' alt='Saisie de votre prénom' size='25' maxlength='25' value='".$donnees['CLI_PRENOM']."' required /></td>";
			echo "</tr>";
			
			echo "<tr>";
			echo "<td><label for='Adresse'>Votre adresse</label></td>";
			echo "<td><input type='text' name='adresse' id='Adresse' alt='Saisie de votre adresse' size='50' maxlength='50' value='".$donnees['CLI_ADRESSE']."' required /></td>";
			echo "</tr>";
			
			echo "<tr>";
			echo "<td><label for='NPA'>Votre code postal</label></td>";
			echo "<td><input type='text' name='npa' id='NPA' alt='Saisie de votre code postal' size='4' maxlength='4' value='".$donnees['CLI_NPA']."' required /></td>";
			echo "</tr>";
			
			echo "<tr>";
			echo "<td><label for='Localite'>Votre localité</label></td>";
			echo "<td><input type='text' name='localite' id='Localite' alt='Saisie de votre localité' size='25' maxlength='25' value='".$donnees['CLI_LOCALITE']."' required /></td>";
			echo "</tr>";
			
			echo "<tr>";
			echo "<td><label for='DoB'>Votre date de naissance</label></td>";			
			$dateDeNaissance = explode("-", $donnees['CLI_DOB']);				
			$dateDeNaissancePourAfficher = $dateDeNaissance[2] . "/" . $dateDeNaissance[1] . "/" . $dateDeNaissance[0];			
			echo "<td><input type='text' name='dob' id='DoB' alt='Saisie de votre date de naissance' size='25' maxlength='25' value='".$dateDeNaissancePourAfficher."' onclick='new calendar(this);' required /></td>";
			echo "</tr>";
			
			echo "<tr>";
			echo "<td><label for='Email'>Votre e-mail</label></td>";
			echo "<td><input type='text' name='email' id='Email' alt='Saisie de votre e-mail' size='50' maxlength='50' value='".$donnees['CLI_EMAIL']."' required /></td>";
			echo "</tr>";
			
			echo "<tr>";
			echo "<td><label for='Mdp'>Votre mot de passe</label></td>";
			echo "<td><input type='password' name='mdp' id='Mdp' alt='Saisie de votre mot de passe' size='50' maxlength='50' value='".$donnees['CLI_MDP']."' required /></td>";
			echo "</tr>";
			
			echo "<tr>";
			echo "<td><label for='ConfirmationMdp'>Confirmez votre mot de passe</label></td>";
			echo "<td><input type='password' name='confirmationmdp' id='ConfirmationMdp' alt='Confirmation de votre mot de passe' size='50' maxlength='50' value='".$donnees['CLI_MDP']."' required /></td>";
			echo "</tr>";
			
			echo "<tr>";
			echo "<td><label for='NumeroTelephone'>Votre numéro de téléphone</label></td>";
			echo "<td><input type='text' name='numeroTelephone' id='NumeroTelephone' alt='Saisie de votre numéro de téléphone' size='25' maxlength='25' value='".$donnees['CLI_NUMERO_TELEPHONE']."' required /></td>";
			echo "</tr>";
			echo "</table><br><br>";
			
			echo "<input type='reset' value='Annuler'>";
			echo "<input type='submit' value='Confirmer'>";
			
			echo "</form>";
			
			$bdd = null;
				
			echo "</div>";
			
		}catch(PDOException $e){
			echo "Erreur !: " . $e->getMessage() . "<br />";
			die();			
		}
	
	?>
	
	<footer class="page-footer font-small bg-dark text-white mt-2 fixed-bottom">
	<div class="footer-copyright text-center py-1">
	  © 2018 Copyright
	</div>
 	</footer>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>