<?php session_start(); ?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Page_DonneesPersonnellesTraitement.php</title>
  </head>
  <body>
    
	<?php
	
		if($_POST['mdp'] == $_POST['confirmationmdp']){
			$idClient = $_SESSION['idClient'];
			
			$nom = $_POST['nom'];
			$prenom = $_POST['prenom'];
			$adresse = $_POST['adresse'];
			$NPA = $_POST['npa'];
			$localite = $_POST['localite'];
			$dateNaissance = $_POST['dob'];
			
			$dateDeNaissance = explode("/",$dateNaissance);
			$dateDeNaissanceSQL = $dateDeNaissance[2]."-".$dateDeNaissance[1]."-".$dateDeNaissance[0];
			
			$email = $_POST['email'];
			$mdp = $_POST['mdp'];
			$numeroTelephone = $_POST['numeroTelephone'];
			
			try{
				
				$bdd = new PDO("mysql:host=hhva.myd.infomaniak.com;dbname=hhva_marcbrnt","hhva_marcbrnt","nFw9BHduqf");
				
				$bdd->query("SET NAMES 'UTF-8'");
				
				$bdd->query("UPDATE client SET CLI_NOM='$nom', CLI_PRENOM='$prenom', CLI_ADRESSE='$adresse', CLI_NPA='$NPA', CLI_LOCALITE='$localite', CLI_DOB='$dateDeNaissanceSQL', CLI_EMAIL='$email', CLI_MDP='$mdp', CLI_NUMERO_TELEPHONE='$numeroTelephone' WHERE CLI_ID = '$idClient'");
				
				$bdd = null;				
				?>

					<script type="text/javascript">
						alert("Vos données ont été modifiées.");
						document.location.href = "Page_Accueil.html";	
					</script>					
				
				<?php			
			}catch(PDOException $e){
				echo "Erreur !: ".$e->getMessage()."<br>";
				die();
			}
			
		}else{ 
			?>
				<script type="text/javascript">
					alert("Les mots de passe que vous avez tapés ne sont pas identiques. Recommencez.");
					document.location.href = "Page_DonneesPersonnelles.php";
				</script>
			<?php
		}
	
	?>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>